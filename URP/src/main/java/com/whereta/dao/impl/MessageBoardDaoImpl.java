package com.whereta.dao.impl;

import com.whereta.dao.IMessageBoardDao;
import com.whereta.mapper.MessageBoardMapper;
import com.whereta.model.MessageBoardWithBLOBs;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Created by vincent on 15-9-24.
 */
@Repository("messageBoardDao")
public class MessageBoardDaoImpl implements IMessageBoardDao {
    @Resource
    private MessageBoardMapper messageBoardMapper;
    //保存留言内容
    @Override
    public int save(MessageBoardWithBLOBs messageBoard) {
        return messageBoardMapper.insertSelective(messageBoard);
    }
}
